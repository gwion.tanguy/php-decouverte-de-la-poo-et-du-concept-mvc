<?php
    class RadioElement extends HTMLElement{
        public function __construct(String $name, String $value = '', String $type = 'text'){
            parent::__construct($name,$value,$type);
        }

        public function __toString(){
            return <<<END
                <label for="$this->value">$this->value</label><br>
                <input type='$this->type' id='$this->value' name='$this->name' value='$this->value'><br>
                END;
        }
    }