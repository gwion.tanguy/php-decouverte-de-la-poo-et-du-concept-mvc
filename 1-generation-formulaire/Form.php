<?php
include 'HTMLElement.php';    
include 'TextElement.php';
include 'RadioElement.php';
include 'CkeckboxElement.php';

class Form {
    public String $action;
    public String $method;

    /**
     * Constructeur de la classe Form
     * 
     * @param String $action
     * @param String $method
     * @return void
     */
    public function __construct(String $action, String $method){
        $this->action = $action;
        $this->method = $method;
        $this->fields = [];
    }

    /**
     * Ajoute un champ de type text
     * 
     * @param String $name
     * @param String $value
     * @return Form
     */
    public function addTextField(String $name, String $value = '') {
        $this->fields[] = new TextElement($name, $value);
        return $this;
    }


    /**
     * Ajoute un champ de type number
     * 
     * @param String $name
     * @param String $value
     * @return Form
     */
    public function addNumberField(String $name, String $value = '') {
        $this->fields[] = new TextElement( $name, $value, 'number');
        return $this;
    }

    /**
     * Ajoute un champ de type radio
     * 
     * @param String $name
     * @param String $value
     * @return Form
     */
    public function addRadioField(String $name, String $value) {
        $this->fields[] = new RadioElement( $name, $value, 'radio');
        return $this;
    }

    /**
     * Ajoute un champ de type checkbox
     * 
     * @param String $name
     * @param String $value
     * @return Form
     */
    public function addCheckboxField(String $name, String $value) {
        $this->fields[] = new CheckboxElement( $name, $value, 'checkbox');
        return $this;
    }
    

    /**
     * Ajoute un champ de type submit
     * 
     * @param String $name
     * @return Form
     */
    public function addSubmitButton(String $name) {
        $this->fields[] = "<input type=\"submit\" name=".$name.">";
        return $this;
    }


    /**
     * Affiche le formulaire
     * 
     * @return void
     */
    public function build() {
        echo "<form action=".$this->action." method=".$this->method.">";

        if (!empty($this->fields)){
            foreach($this->fields as $f){
                echo $f;
            }
        }

        echo "</form>";
    }

}