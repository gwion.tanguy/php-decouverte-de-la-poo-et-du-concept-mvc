<?php
    abstract class HTMLElement{
        public function __construct(String $name, String $value = '', String $type = 'text'){
            $this->name = $name;
            $this->value = $value;
            $this->type = $type;
        }

        public function __toString(){
            return <<<END
                <label for="$this->name">$this->name</label><br>
                <input type='$this->type' name='$this->name' value='$this->value'><br>
                END;
        }
    }