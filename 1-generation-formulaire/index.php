<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Index</title>
</head>
<body>
    <?php
        require 'Form.php';

        $form = new Form('#', 'GET');  // créer le début du formulaire
        $form->addTextField('lastname') // créer un input de type texte avec comme valeur $lastname
            ->addTextField('firstname') // créer un input de type texte avec comme valeur $firstname
            ->addNumberField('age')// créer un input de type nombre avec comme valeur $age
            ->addRadioField('Sexe', 'M')// créer un input de type radio avec comme valeur $sexe
            ->addRadioField('Sexe', 'F')// créer un input de type radio avec comme valeur $sexe
            ->addCheckboxField('Langage','PHP')// créer un input de type checkbox avec comme valeur $langage
            ->addCheckboxField('Langage','Javascript')// créer un input de type checkbox avec comme valeur $langage
            ->addCheckboxField('Langage','Python')// créer un input de type checkbox avec comme valeur $langage
            ->addSubmitButton('Save'); //Créer un bouton pour soumettre le formulaire se nommant Save

        echo $form->build(); // générer le formulaire
    ?>
    
</body>
</html>