---
title: PHP - La découverte de la POOen créant notre propre MVC.
metadata:
  level : second_year, third_year
  language: php
---
# Exercices PHP -La Programmation orientée objet

Il vous est demandé de forker ce dépot, réaliser le travail demandé puis de faire une merge request en fin de journée.
Tâchez d'aller le plus loin possible...Ce travail servira de base pour le travail de la journée suivante.
Nous ferons un point en début d'après-midi puis en fin de journée.

> Rappel : un fichier PHP ne s'ouvre dans le navigateur en faisant un glisser déposer ! Ça ne va pas fonctionner !
Vous devez absolument passer par votre serveur apache local : http://localhost/ ou http://localhost:8000/ ou etc.
> Voire en lançant un serveur depuis le dossier par le biais de la commande : `php -S localhost:8000`.

# Jour 1
Tout se trouve dans le dossier [1-generation-formulaire](./1-generation-formulaire)

Bon courage à tou.te.s
